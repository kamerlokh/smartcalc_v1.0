#ifndef SRC_POLISH_H_
#define SRC_POLISH_H_
// #include <error.h>
#define rTRUE    1
#define rFALSE   0

#define ERROR_OK            0
#define ERROR_PARENTHESIS   1
#define ERROR_IN_EXPRESSION 2
#define ERROR_OPERATION     3
#define ERROR_FUNCTION      4
#define ERROR_DIV_BY_ZERO   5
#define ERROR_SQRT_MINUS    6
#define ERROR_LN_MINUS      7
#define ERROR_LOG_MINUS     8


#define uchar unsigned char

#define PI acos(-1)


r_stack_t *parse(char *input, int *error);
char    *is_function_or_left_brace(char *input, char prev_token);
r_stack_t *add_number_to_queue(r_stack_t *queue, char *input);
int      is_function(char *input);
char    *is_number(char *input);
char     is_operation(char *input/*, char prev_token*/);
int      process_right_parenthesis(r_stack_t **st, r_stack_t **queue);
int      is_unary(char op);

double calc(r_stack_t *polish, double x, int *error);
r_stack_t *process_binary_calc(r_stack_t *tmp, char *operation_name, double x, int *error);
r_stack_t *process_unary_calc(r_stack_t *tmp, char *function_name, double x, int *error);
double   str_to_double(char *str, double x);
char    *double_to_str(double d);
double   do_binary_operate(char *operation_name, double first, double two, int *error);
double   do_unary_operate(char *function_name, double first, int *error);
double parse_and_calc(char *exp, int *error);
char *get_error_text(int error);

#define UNUSED(x) (void)(x)

#endif  // SRC_POLISH_H_
