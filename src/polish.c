#include "stack.h"
#include "polish.h"
#include <stdio.h>
#include <string.h>
#include <math.h>

static const int    functions_count = 11;
static const char  *functions[] = {"cos", "sin", "tan", "acos", "asin",
                                   "atan", "sqrt", "ln", "log", "~", "#"};
static const char  *operations = "+-*/^%";
static const unsigned char op_priority[128] = {['+'] = 5,
                                               ['-'] = 5,
                                               ['*'] = 3,
                                               ['/'] = 3,
                                               ['^'] = 1,
                                               ['%'] = 4,
                                               ['#'] = 2,
                                               ['~'] = 2};


r_stack_t *parse(char *input, int *error) {
    *error = ERROR_OK;
    r_stack_t *st = NULL;
    r_stack_t *queue = NULL;
    char *tmp;
    char *function_or_lb;
    char prev_token = '\0';
    char current_op;  //  = '\0';
    while (input[0] != '\0') {
        char *number;
        if (strlen(number = is_number(input)) > 0) {  // A number – put it into the output queue
            queue = pop(queue, number);
            input += strlen(number);
            // A function or a left parenthesis – push it onto the stack
        } else if (strlen(function_or_lb = is_function_or_left_brace(input, prev_token)) != 0) {
            st = pop(st, function_or_lb);
            input += strlen(function_or_lb);
        } else if ((current_op = is_operation(input/*, prev_token*/)) != '\0') {  // Operator (O1)
            char tmp2[2] = {current_op, '\0'};
            if (is_unary(current_op) == rTRUE) {
                queue = pop(queue, tmp2);
            } else {
                while ((tmp = peek(st)) != NULL &&
                        op_priority[(uchar)tmp[0]] > 0 &&   // Current op is op
                        // While there is an token-operator O2 at the top of the stack,
                        // that has greater precedence than O1
                        op_priority[(uchar)tmp[0]] <= op_priority[(uchar)current_op]) {
                    tmp = push(&st);
                    queue = pop(queue, tmp);  // Pop O2 from the stack into the output queue
                }
                st = pop(st, tmp2);  // Push O1 onto the stack
            }
            input++;
        } else if (input[0] == ')') {
            *error = process_right_parenthesis(&st, &queue);
            input++;
        } else {
            input++;
        }
        prev_token = input[-1];
    }
    if (st != NULL && strchr(peek(st), '(') != NULL  && *error == ERROR_OK)
        *error = ERROR_IN_EXPRESSION;

    while (((tmp = push(&st))[0] != '\0') && (*error == ERROR_OK)) {
        queue = pop(queue, tmp);
    }
    free_stack(st);
    return queue;
}


char *is_function_or_left_brace(char *input, char prev_token) {
    static char ret[MAX_ARR];
    ret[0] = '\0';
    if (input != NULL) {
        if (input[0] == '(') {
            snprintf(ret, sizeof(ret), "%s", "(\0");
            // strcpy(ret, "(\0");
        } else if ((prev_token == '\0' ||
                    prev_token == '(' || strchr(operations, prev_token) != NULL) &&
                    (input[0] == '-' || input[0] == '+')) {
            if (input[0] == '-' )
                snprintf(ret, sizeof(ret), "%s", "~");
                // strcpy(ret, "~");
            else if (input[0] == '+' )
                snprintf(ret, sizeof(ret), "%s", "#");
                // strcpy(ret, "#");
        } else {
            for (int i = 0; i < functions_count && strlen(ret) == 0; i++) {
                int current_function_len = strlen(functions[i]);
                if (strncmp(input, functions[i], current_function_len) == 0)
                    snprintf(ret, sizeof(ret), "%s", functions[i]);
                    // strcpy(ret, functions[i]);
            }
        }
    }
    return ret;
}

//  возвращает символы в номере
char *is_number(char *input) {
    static char ret[MAX_ARR] = "";
    if (input[0] == 'p' && input[1] == 'i') {
        // strcpy(ret, "pi");
        snprintf(ret, sizeof(ret), "%s", "pi");
    } else {
        int cnt = 0;
        while ((input[0] >= '0' && input[0] <= '9') || input[0] == '.' || input[0] == 'x') {
            ret[cnt] = input[0];
            input++;
            cnt++;
        }
        ret[cnt]  = '\0';
    }
    return ret;
}


/**  унарные это значит, или сейчас начало выражения
 * или предыдущий токен - открывающая скобка
 * или предыдущий токен - операция
*/
char is_operation(char *input/*, char prev_token*/) {
    char ret = '\0';
    if (strchr(operations, input[0]) != NULL) {
        /*if (prev_token == '\0' || prev_token == '(' || strchr(operations, prev_token) != NULL) {
            if (input[0] == '-' )
                ret = '~';
            else if (input[0] == '+' )
                ret = '#';
            else 
                ret = input[0];
        } else {*/
            ret = input[0];
        /*}*/
    }
    return ret;
}



int process_right_parenthesis(r_stack_t **st, r_stack_t **queue) {
    int ret = ERROR_OK;
    char *tmp;
    int left_parenthesis_readed = 0;
    // While the token at the top of the stack is not a left parenthesis,
    // pop the token-operators from the stack into the output queue.
    while ((left_parenthesis_readed = (*st != NULL) ) && (strchr(tmp = push(st), '(') == NULL)) {
        *queue = pop(*queue, tmp);              // Pop the left parenthesis from the stack and discard it.
    }
    if (left_parenthesis_readed == 0) {
        // нарушено сочитаемость скобок
        ret = ERROR_PARENTHESIS;
        //  If the stack ended before the left parenthesis was read, there is an error in the expression.
    } else {
        // If there is a function token at the top of the stack,
        // then pop the function from the stack into the output queue
        if ((tmp = peek(*st)) != NULL &&
                strlen(is_function_or_left_brace(tmp, '\0')) > 0 &&
                (strchr(tmp, '(') == NULL)) {
            tmp = push(st);
            *queue = pop(*queue, tmp);
        }
    }
    return ret;
}

int is_unary(char op) {
    int ret = rFALSE;
    if (op == '~' || op == '#')
        ret = rTRUE;
    return ret;
}

double calc(r_stack_t *polish, double x, int *error) {
    r_stack_t *tmp_polish = stack_invert(polish);
    r_stack_t *tmp = NULL;
    while (tmp_polish != NULL) {
        char *current_op = push(&tmp_polish);
        if (is_number(current_op)[0] != '\0') {
            tmp = pop(tmp, current_op);
        } else if (is_operation(current_op/*, '\0'*/) != '\0') {
            if (is_unary(current_op[0]) == rFALSE)
                tmp = process_binary_calc(tmp, current_op, x, error);
            else
                tmp = process_unary_calc(tmp, current_op, x, error);
        } else if (is_function_or_left_brace(current_op, '\0')[0] != '\0') {
            tmp = process_unary_calc(tmp, current_op, x, error);
        }
    }
    return str_to_double(push(&tmp), x);
}

r_stack_t *process_binary_calc(r_stack_t *tmp, char *operation_name, double x, int *error) {
    //  r_stack_t *ret = tmp;
    char op[MAX_ARR];
    snprintf(op, sizeof(op), "%s", operation_name);
    // strcpy(op, operation_name);
    double first = str_to_double(push(&tmp), x);
    double two = str_to_double(push(&tmp), x);
    double res = do_binary_operate(op, first, two, error);
    if (*error == ERROR_OK) {
        char *res_str = double_to_str(res);
        tmp = pop(tmp, res_str);
    }
    return tmp;
}

r_stack_t *process_unary_calc(r_stack_t *tmp, char *function_name, double x, int *error) {
    //  r_stack_t *ret = tmp;
    char op[MAX_ARR];
    snprintf(op, sizeof(op), "%s", function_name);
    // strcpy(op, function_name);
    double first = str_to_double(push(&tmp), x);
    double res = do_unary_operate(op, first, error);
    if (*error == ERROR_OK) {
        char *res_str = double_to_str(res);
        tmp = pop(tmp, res_str);
    }
    return tmp;
}

double str_to_double(char *str, double x) {
    double ret = .0;
    if (str != NULL) {
        if (str[0] == 'x' && str[1] == '\0')  // это x
            ret = x;
        else if (strcmp(str, "pi") == 0)  // это pi
            ret = PI;
        else
            sscanf(str, "%lf", &ret);
    }

    return ret;
}

char *double_to_str(double d) {
    static char ret[MAX_ARR];
    ret[0] = '\0';
    snprintf(ret, sizeof(ret), "%.7lf", d);
    return ret;
}

// +-*/^%
double do_binary_operate(char *operation_name, double first, double two, int *error) {
    double ret = .0;
    *error = ERROR_OK;
    if (operation_name[0] == '+') {
        ret = first + two;
    } else if (operation_name[0] == '-') {
        ret = two - first;
    } else if (operation_name[0] == '*') {
        ret = first * two;
    } else if (operation_name[0] == '/') {
        if (first == 0.)
            *error = ERROR_DIV_BY_ZERO;
        ret = two / first;
    } else if (operation_name[0] == '^')  {
        ret = pow(two, first);
    } else if (operation_name[0] == '%') {
        ret = fmod(two, first);
    } else {
        *error = ERROR_OPERATION;
    }
    return ret;
}
//  {"cos","sin","tan","acos","asin","atan","sqrt","ln","log"};  ~  #
double do_unary_operate(char *function_name, double first, int *error) {
    double ret = .0;
    *error = ERROR_OK;
    if (function_name[0] == '#') {
        ret = first;
    } else if (function_name[0] == '~') {
        ret = -first;
    } else if (strcmp(function_name, "cos") == 0) {
        ret = cos(first);
    } else if (strcmp(function_name, "sin") == 0) {
        ret = sin(first);
    } else if (strcmp(function_name, "tan") == 0) {
        ret = tan(first);
    } else if (strcmp(function_name, "acos") == 0) {
        ret = acos(first);
    } else if (strcmp(function_name, "asin") == 0) {
        ret = asin(first);
    } else if (strcmp(function_name, "atan") == 0) {
        ret = atan(first);
    } else if (strcmp(function_name, "sqrt") == 0) {
        if (first < 0)
            *error = ERROR_SQRT_MINUS;
        else
            ret = sqrt(first);
    } else if (strcmp(function_name, "ln") == 0) {
        if (first <= 0)
            *error = ERROR_LN_MINUS;
        else
            ret = log(first);
    } else if (strcmp(function_name, "log") == 0) {
        if (first <= 0)
            *error = ERROR_LOG_MINUS;
        else
            ret = log10(first);
    } else {
        *error = ERROR_FUNCTION;
    }
    return ret;
}


#ifndef DEBUG



double parse_and_calc(char *exp, int *error) {
    double ret = 0.;
    r_stack_t *tmp = parse(exp, error);
    if (*error == ERROR_OK)
        ret = calc(tmp, 16, error);
    free_stack(tmp);
    return ret;
}

char *get_error_text(int error) {
    char *ret;
    int error_count = 10;
    /** char *arr[] = {"Успешное выполнение", 
                    "Нарушение сочетаемости скобок", 
                    "Ошибка в выражении", 
                    "Неизвестная операция", 
                    "Неизвестная функция", 
                    "Деление на ноль", 
                    "Корень из отрицательного числа", 
                    "Логарифм отрицательного числа", 
                    "Логарифм отрицательного числа"
                    "Неизвестная ошибка"};*/
    char *arr[] = {"Evaluation is it OK",
                    "Error parenthesis count",
                    "Error in expression",
                    "Unknown operation",
                    "Unknown function",
                    "Division by zero",
                    "SQRT of negative number",
                    "LN of non positive number",
                    "LOG of non positive number",
                    "Unknown error"};

    if (error < 0 || error >= error_count)
        ret = arr[error_count - 1];
    else
        ret = arr[error];
    printf("%s", ret);
    return ret;
}
#endif
