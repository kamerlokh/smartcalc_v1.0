#include "../stack.h"
#include "../polish.h"



#include <check.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>


START_TEST(test_s21_polish) {
    #define strings_cnt 23
    int error;
    int x = 16;
    double res;
    char *strings[] = {
                        "1-1",
                        "2/2",
                        "-2/(3+2)*5",
                        "1+1",
                        "+-5*-2/x+-((5-(-(x)+(30))))+((x))",
                        "-5*-2/sin(x)/cos(5)+-tan(atan(5-sqrt(ln(x)+log(30))))",
                        "-",
                        "5^5",
                        "5^-5",
                        "5%1",
                        "5%3",
                        "5/0",
                        "acos(x)",
                        "acos(1)",
                        "asin(x)",
                        "asin(1)",
                        "sqrt(x)",
                        "sqrt(-1)",
                        "ln(-1)",
                        "log(-1)",
                        "1-(",
                        "1-()",
                        "1-())"
                        };
    int errors[]     = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ERROR_DIV_BY_ZERO, 0, 0, 0, 0, 0,
                    ERROR_SQRT_MINUS, ERROR_LN_MINUS, ERROR_LOG_MINUS,
                    ERROR_IN_EXPRESSION, 0, ERROR_PARENTHESIS};
    double results[] = {0, 1, -2, 2, 25.625000, -125.3865726, 0, 3125, 0.00032, 0,
                        2, 0, acos(x), acos(1), asin(x), asin(1), -5, 0, 0, 0, 1, 1, 0};
    for (int i = 0; i < strings_cnt; i++) {
        r_stack_t *tmp = parse(strings[i], &error);
        if (error == 0)
            res = calc(tmp, x, &error);
        if (error != errors[i]) {
            free_stack(tmp);
            ck_abort_msg("Error result for string %s. Need error result = %d. Counted error result = %d",
                        strings[i], errors[i], error);
        } else {
            if (res != results[i] && (isnan(res) != isnan(results[i]))) {
                free_stack(tmp);
                ck_abort_msg("Error calculate for string %s. Need result = %.7lf. Counted result = %.7lf.",
                            strings[i], results[i], res);
            }
        }
        free_stack(tmp);
    }
}
END_TEST


int main(void) {
    Suite *s1 = suite_create("Core");
    TCase *tc1_1 = tcase_create("Core");
    SRunner *sr = srunner_create(s1);
    int nf;
    srunner_set_fork_status(sr, CK_NOFORK);
    suite_add_tcase(s1, tc1_1);


    tcase_add_test(tc1_1, test_s21_polish);

    srunner_run_all(sr, CK_ENV);
    nf = srunner_ntests_failed(sr);
    srunner_free(sr);
    return nf == 0 ? 0 : 1;
}
