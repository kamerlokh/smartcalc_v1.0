#include <cairo.h>
#include <gtk/gtk.h>
#include <math.h>
#include "stack.h"
#include "gui.h"
#include "polish.h"

static char *btn_names[] = {"cos", "sin", CE, BACK_SPACE, "tan", "atan", "asin", "acos", "sqrt", "ln", "log",
                            "^", "%", "x", "(", ")", "/", "7", "8", "9", "*", "4", "5", "6", "-", "1",
                            "2", "3", "+", ".", "0", "=", GRAPH};
static int ERROR_IN_STACK = FALSE;

#ifndef DEBUG
int main() {
    init_gtk(&window);
    init_calc_text(&calc_text);
    init_layout(window, &v_box, calc_text, &button_container);
    init_buttons(button_container, button);
    gtk_widget_show_all(window);
    g_signal_connect(G_OBJECT(window), "destroy", G_CALLBACK(gtk_main_quit), NULL);
    gtk_main();
    return 0;
}
#endif

void init_calc_text(GtkWidget **calc_text) {
    *calc_text = gtk_entry_new();
    gtk_entry_set_max_length(GTK_ENTRY(*calc_text), 255);
    gtk_entry_set_alignment(GTK_ENTRY(*calc_text), 1);
    gtk_editable_set_editable(GTK_EDITABLE(*calc_text), FALSE);

    g_signal_connect(G_OBJECT(*calc_text),   "insert_text", G_CALLBACK(insert_text), NULL);
    g_signal_connect(G_OBJECT(*calc_text),   "key_press_event", G_CALLBACK(escape), NULL);
    g_signal_connect(G_OBJECT(*calc_text),   "backspace", G_CALLBACK(backspace), NULL);
}

void init_buttons(GtkWidget *window, GtkWidget *button[]) {
    for (int i = 0; i < BTN_COUNT; i++) {
        button[i] = gtk_button_new_with_label(btn_names[i]);
        gtk_grid_attach(GTK_GRID(window), button[i], i%4*60, i/4*10, 40, 10);
        g_signal_connect(G_OBJECT(button[i]), "clicked", G_CALLBACK(button_click), NULL);
    }
}

void init_layout(GtkWidget *window, GtkWidget **v_box, GtkWidget *calc_text, GtkWidget **button_container) {
    *v_box = gtk_box_new(GTK_ORIENTATION_VERTICAL, 10);
    *button_container = gtk_grid_new();
    gtk_container_add(GTK_CONTAINER(*v_box), calc_text);
    gtk_container_add(GTK_CONTAINER(*v_box), *button_container);
    gtk_container_add(GTK_CONTAINER(window), *v_box);
}


void init_gtk(GtkWidget **window) {
    gtk_init(NULL, NULL);
    *window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title(GTK_WINDOW(*window), "Р-Калькулятор");
    GtkCssProvider *cssProvider = gtk_css_provider_new();
    gtk_css_provider_load_from_path(cssProvider, "gui.css", NULL);
    gtk_style_context_add_provider_for_screen(gdk_screen_get_default(),
                               GTK_STYLE_PROVIDER(cssProvider),
                               GTK_STYLE_PROVIDER_PRIORITY_USER);
}

void insert_text(GtkEditable* self, gchar* text, gint length, gint* position, gpointer user_data) {
    UNUSED(text); UNUSED(length); UNUSED(position); UNUSED(user_data);
    processing_input_text(text);
    g_signal_stop_emission_by_name(self, "insert-text");
}

void backspace(GtkEntry* self, gpointer user_data) {
    UNUSED(self); UNUSED(user_data);
    processing_input_text(BACK_SPACE);
}

void escape(GtkWidget* self, GdkEventKey *event, gpointer user_data) {
    UNUSED(self); UNUSED(user_data);
    //  printf ("%d\t%d\n",event->keyval, GDK_KEY_Return);
    if (event->keyval == GDK_KEY_Escape) {
        processing_input_text(CE);
        g_signal_stop_emission_by_name(self, "key_press_event");
    }

    if (event->keyval == GDK_KEY_KP_Enter || event->keyval == GDK_KEY_Return) {
        processing_input_text("=");
    } else {
        processing_input_text(event->string);
    }
}

int is_input_right(char *text) {
    int ret = FALSE;
    if (text != NULL && text[0] != '\0') {
        for (int i = 0; i < BTN_COUNT && ret == FALSE; i++) {
            if (strcmp(btn_names[i], text) == 0)
                ret = TRUE;
        }
    }
    return ret;
}

void button_click(GtkButton* self, gpointer user_data) {
    UNUSED(user_data);
    gchar *label = (gchar *)gtk_button_get_label(self);
    processing_input_text(label);
}

void processing_input_text(gchar* text) {
    if (strcmp(text, CE) == 0) {
        ERROR_IN_STACK = FALSE;
        free_stack(calc_text_st);
        calc_text_st = NULL;
    } else if (ERROR_IN_STACK == FALSE) {
        if (strcmp(text, BACK_SPACE) == 0) {
            if (calc_text_st != NULL)
                push(&calc_text_st);
        } else if (strcmp(text, "=") == 0) {
            process_calculate();
        } else if (strcmp(text, GRAPH) == 0) {
            plot_graphic();
        } else if (is_input_right(text) == TRUE) {
            calc_text_st = pop(calc_text_st, text);
        }
    }

    put_r_stack_to_entry();
    gtk_entry_grab_focus_without_selecting(GTK_ENTRY(calc_text));
    gtk_editable_set_position(GTK_EDITABLE(calc_text), -1);
}

void put_r_stack_to_entry() {
    char arr[255]="\0";

    print_r_stack_to_arr(calc_text_st, arr);
    set_text_to_calc_text(arr);
}

void process_calculate() {
    ERROR_IN_STACK = FALSE;
    char arr[255]="\0";
    int error = ERROR_OK;
    print_r_stack_to_arr(calc_text_st, arr);
    double ret = parse_and_calc(arr, &error);

    free_stack(calc_text_st);
    if (error != ERROR_OK) {
        snprintf(arr, sizeof(arr), "%s", get_error_text(error));
        ERROR_IN_STACK = TRUE;
    } else {
        snprintf(arr, sizeof(arr), "%.7lf", ret);
        if (isnan(ret))
            ERROR_IN_STACK = TRUE;
    }
    calc_text_st = pop(NULL, arr);
}

void set_text_to_calc_text(char *str) {
    guint len_arr = strlen(str);
    GtkEntryBuffer *tmp = gtk_entry_get_buffer(GTK_ENTRY(calc_text));
    gtk_entry_buffer_set_text(tmp, str, len_arr);
}

void plot_graphic() {
    GError *error = NULL;

    GtkBuilder *builder = gtk_builder_new();

    // загрузка пользовательского интерфеса из файла, который мы создали в Glade
    if (!gtk_builder_add_from_file(builder, "plot_window.glade", &error)) {
        g_warning("%s", error->message);
        g_free(error);
    } else {
        plot_window  = GTK_WIDGET(gtk_builder_get_object(builder, "plot_window"));
        draw_window = GTK_WIDGET(gtk_builder_get_object(builder, "draw_window"));
        g_signal_connect(G_OBJECT(draw_window), "draw", G_CALLBACK(on_draw), NULL);

        gtk_widget_show(plot_window);
    }
    g_object_unref(G_OBJECT(builder));
}


void draw_axis(cairo_t *cr) {
    gdouble clip_x1 = 0.0, clip_y1 = 0.0, clip_x2 = 0.0, clip_y2 = 0.0;
    cairo_set_font_size(cr, 13);
    cairo_clip_extents(cr, &clip_x1, &clip_y1, &clip_x2, &clip_y2);

    // axis
    cairo_set_source_rgb(cr, 0.0, 0.0, 0.0);
    cairo_set_line_width(cr, 0.7);
    cairo_move_to(cr, clip_x1, (clip_y2 - clip_y1)/2);
    cairo_line_to(cr, clip_x2, (clip_y2 - clip_y1)/2);
    cairo_move_to(cr, (clip_x2 - clip_x1)/2, clip_y1);
    cairo_line_to(cr, (clip_x2 - clip_x1)/2, clip_y2);
    cairo_stroke(cr);

    // axis symbol
    cairo_set_source_rgb(cr, 0.0, 0.0, 0.0);
    cairo_move_to(cr, clip_x2 - 10, (clip_y2 - clip_y1)/2 - 5);
    cairo_show_text(cr, "x");
    cairo_move_to(cr, (clip_x2 - clip_x1)/2-10, clip_y1+10);
    cairo_show_text(cr, "y");
    cairo_stroke(cr);
    // setka
    gdouble line_width = cairo_get_line_width(cr);
    cairo_set_line_width(cr, 0.1);
    cairo_set_font_size(cr, 10);
    for (int i = 1; i < 26; i ++) {
        if ((i - 13)%5 == 0) {
            char buf[3];
            snprintf(buf, sizeof(buf), "%d", i - 13);

            cairo_text_extents_t extents;
            cairo_text_extents(cr, buf, &extents);
            double smech;
            if (i-13 == 0)
                smech = 10;
            else
                smech = extents.width/2 + extents.x_bearing;
            cairo_move_to(cr, clip_x1 + i * (clip_x2 - clip_x1)/26 - smech,
                             (clip_y2 - clip_y1)/2 + 10);
            cairo_show_text(cr, buf);
            if (i - 13 != 0) {
                cairo_move_to(cr, (clip_x2 - clip_x1)/2 - 5 - extents.width,
                                   clip_y1 + i * (clip_y2 - clip_y1)/26);
                cairo_show_text(cr, buf);
            }
        }
        cairo_move_to(cr, clip_x1 + i * (clip_x2 - clip_x1)/26, clip_y1);
        cairo_line_to(cr, clip_x1 + i * (clip_x2 - clip_x1)/26, clip_y2);

        cairo_move_to(cr, clip_x1, clip_y1 + i * (clip_y2 - clip_y1)/26);
        cairo_line_to(cr, clip_x2, clip_y1 + i * (clip_y2 - clip_y1)/26);
    }
    cairo_stroke(cr);
    cairo_set_line_width(cr, line_width);
}

void draw_func(cairo_t *cr) {
    char arr[255]="\0";
    int error = ERROR_OK;

    gdouble clip_x1 = 0.0, clip_y1 = 0.0, clip_x2 = 0.0, clip_y2 = 0.0;
    cairo_clip_extents(cr, &clip_x1, &clip_y1, &clip_x2, &clip_y2);

    gdouble height = clip_y2 - clip_y1;
    gdouble width  = clip_x2 - clip_x1;

    cairo_set_source_rgb(cr, 1, 0.1, 0.1);
    cairo_set_line_width(cr, 1);
    print_r_stack_to_arr(calc_text_st, arr);

    r_stack_t *tmp = parse(arr, &error);

    gdouble prev = 0.;

    if (error == ERROR_OK) {
        for (double x = -13; x <= 13; x += 0.01) {
            gdouble calulated = calc(tmp, x, &error);
            int is_first = rTRUE;
            if (is_first == rTRUE || fabs(calulated - prev) > 150) {
                cairo_move_to(cr, clip_x1 + (13 + x)*(width/26),
                                  (height/2) - calc(tmp, x, &error) * (height/2)/13);
                is_first = rFALSE;
            } else {
                cairo_line_to(cr, clip_x1 + (13 + x)*(width/26),
                                  (height/2) - calc(tmp, x, &error) * (height/2)/13);
            }
            prev = calulated;
        }
    }
    cairo_stroke(cr);
    free_stack(tmp);
}

void on_draw(GtkWidget *widget, cairo_t *cr, gpointer user_data) {
    UNUSED(user_data);
    GdkRectangle da;
    GdkWindow *tmp_plot_window = gtk_widget_get_window(widget);
    gdk_window_get_geometry(tmp_plot_window,
            &da.x,
            &da.y,
            &da.width,
            &da.height);
    /* Draw on a black background */
    cairo_set_source_rgb(cr, 0.1, 0.1, 0.1);
    draw_axis(cr);
    draw_func(cr);
    cairo_move_to(cr, 20, 30);
    cairo_set_source_rgba(cr, 1, 0.2, 0.2, 0.6);
    cairo_stroke(cr);
}
