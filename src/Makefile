prj 			:= 	s21_calc
version			:=	1.0
CFLAGS			:=	-std=c11 -Wall -Werror -Wextra
gcov 			:= 	--coverage -g
gcc_cc			:=	gcc $(CFLAGS)
gcc_gcov 		:=	$(gcc_cc) $(gcov)  -D DEBUG
compiler		:=	$(gcc_cc) -g
gcc_test		:=	$(compiler) -D DEBUG
build_dir		:=
# определяем ОС специфед -lcheck
UNAME := $(shell uname)
ifeq ($(UNAME), Linux)
	LGTK = $(shell pkg-config --cflags --libs gtk+-3.0)
	LCHECK = $(shell pkg-config --cflags --libs check)
	LEAKS_CMD = CK_FORK=no valgrind  --tool=memcheck --leak-check=full --track-origins=yes 
endif
ifeq ($(UNAME), Darwin)
	LCHECK = -lcheck
	LEAKS_CMD = CK_FORK=no leaks --atExit -- 
endif


.PHONY:	$(prj) gcov_report clean test s21_math.a gcov_report $(gcc_gcov) install uninstall
.IGNORE: $(prj) cppcheck cpplint s21_test.out report/s21_test.out 

all_c_files 	:= polish.c stack.c
all_o_files		:= $(patsubst %.c, %.o, $(all_c_files))

all: clean $(prj) 

clean:
	rm -rf $(prj) *.o *.a *.out tests/*.info tests/*.out *.gc* tests/rm/* report/* s21_calc.dvi s21_calc.log s21_calc.aux

$(prj):
	mkdir $(build_dir)
	$(compiler) $(all_c_files) gui.c -o $(build_dir)$(prj) $(LGTK) -lm

cpplint:
	cp ../materials/linters/CPPLINT.cfg CPPLINT.cfg
	python3 ../materials/linters/cpplint.py --extensions=c *.c *.h
	python3 ../materials/linters/cpplint.py --extensions=c tests/*.c tests/*.h
	rm -rf CPPLINT.cfg

cppcheck: 
	cppcheck --enable=all --suppress=missingIncludeSystem *.c *.h

check: cpplint cppcheck

test: clean $(prj) cpplint 
	$(gcc_test) tests/s21_test.c $(all_c_files) $(LCHECK) $(LGTK)
	./a.out

gcov_report: compiler:=$(gcc_gcov)

gcov_report: build_dir:=report/

gcov_report: clean $(prj_template)
	$(gcc_gcov) $(all_c_files) tests/s21_test.c -o $(build_dir)s21_test.out $(LCHECK) $(LGTK)
	$(LEAKS_CMD) $(build_dir)s21_test.out  || true
	lcov -t "s21_math" -o $(build_dir)s21_test.info -c -d .
	genhtml -o report $(build_dir)s21_test.info

install:
	cp ./s21_calc /usr/local/bin
uninstall:
	rm -rf /usr/local/bin/s21_calc
dvi:
	texi2dvi s21_calc.tex
	texi2pdf s21_calc.tex

dist: build_dir:=$(prj)-$(version)/
dist: all dvi
	cp plot_window.glade s21_calc.dvi s21_calc.pdf $(prj)-$(version)/
	tar cfz $(prj)-$(version).tar.gz $(prj)-$(version)/
	rm -rf $(prj)-$(version) s21_calc.dvi s21_calc.log s21_calc.aux s21_calc.pdf