#ifndef SRC_STACK_H_
#define SRC_STACK_H_

#define MAX_ARR 256

typedef struct stack_struct r_stack_t;

struct stack_struct {
    char element[100];
    int  element_type_t;
    r_stack_t *next;
};
typedef enum {
    EL_OPERATION = 0,
    EL_FUNCTION = 1,
    EL_UNARY = 2,
    EL_SYMBOL = 3
} element_type_t;

r_stack_t     *pop(r_stack_t *st, char *txt);
char        *push(r_stack_t **st);
char        *peek(r_stack_t *st);
void         free_stack(r_stack_t *st);
void         print_stack(r_stack_t *st);
r_stack_t     *stack_invert(r_stack_t *src);
void         print_r_stack_to_arr(r_stack_t *st, char arr[]);
#endif  // SRC_STACK_H_
