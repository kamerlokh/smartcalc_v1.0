#include "stack.h"
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

/**
 * @brief всунуть
 * если st == NULL, то инит стек
 */
r_stack_t *pop(r_stack_t *st, char *txt) {
    r_stack_t *ret = malloc(sizeof(r_stack_t));
    if (ret != NULL) {
        snprintf(ret->element, sizeof(ret->element), "%s", txt);
        // strcpy(ret->element, txt);
        ret->next = st;
    }
    return ret;
}

// высунуть
char *push(r_stack_t **st) {
    static char ret[MAX_ARR];
    ret[0] = '\0';
    if (*st != NULL) {
        r_stack_t *tmp = *st;
        snprintf(ret, sizeof(ret), "%s", tmp->element);
        // strcpy(ret, tmp->element);
        if (tmp->next != NULL)
            *st = tmp->next;
        else
            *st = NULL;
        if (tmp != NULL)
            free(tmp);
    }
    return ret;
}

char *peek(r_stack_t *st) {
    char *ret = NULL;
    if (st != NULL) {
        ret = st->element;
    }
    return ret;
}

r_stack_t *stack_invert(r_stack_t *src) {
    r_stack_t *ret = NULL;
    r_stack_t *tmp = src;
    while (tmp != NULL) {
        ret = pop(ret, tmp->element);
        tmp = tmp->next;
    }
    return ret;
}

void free_stack(r_stack_t *st) {
    if (st!= NULL) {
        if (st->next != NULL) {
            free_stack(st->next);
            st->next = NULL;
        }
        free(st);
        // st = NULL;
    }
}

#ifndef DEBUG
void print_stack(r_stack_t *st) {
    if (st->next != NULL)
        print_stack(st->next);
    if (st != NULL) {
        printf("%s ", st->element);
    }
}

void print_r_stack_to_arr(r_stack_t *st, char arr[]) {
    if (st != NULL && st->next != NULL)
        print_r_stack_to_arr(st->next, arr);
    if (st != NULL) {
        snprintf(arr + strlen(arr), sizeof(st->element), "%s", st->element);
        // strcat(arr, st->element);
    }
}

#endif
