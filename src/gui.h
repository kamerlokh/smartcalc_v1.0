#ifndef SRC_GUI_H_
#define SRC_GUI_H_

#define BTN_COUNT 33
#define BACK_SPACE "\342\214\253"
#define CE "CE"
#define GRAPH "GRAPH"
    /* Объявляем виджеты */
    GtkWidget *button[BTN_COUNT];   //  Кнопки
    GtkWidget *window;  // Главное окно
    GtkWidget *plot_window;  //  Граф окно
    GtkWidget *v_box;
    GtkWidget *calc_text;
    GtkWidget *button_container;
    GtkWidget *draw_window;

    r_stack_t     *calc_text_st = NULL;

void    init_calc_text(GtkWidget **calc_text);
void    init_buttons(GtkWidget *window, GtkWidget *button[]);
void    init_layout(GtkWidget *window, GtkWidget **v_box, GtkWidget *calc_text, GtkWidget **button_container);
void    init_gtk(GtkWidget **window);
void    insert_text(GtkEditable* self, gchar* text, gint length, gint* position, gpointer user_data);
void    backspace(GtkEntry* self, gpointer user_data);
void    escape(GtkWidget* self, GdkEventKey *event, gpointer user_data);
int     is_input_right(char *text);
void    button_click(GtkButton* self, gpointer user_data);
void    processing_input_text(gchar* text);
void    put_r_stack_to_entry();
void    process_calculate();
void    set_text_to_calc_text(char *str);
void    plot_graphic();
void    on_draw(GtkWidget *widget, cairo_t *cr, gpointer user_data);

#define UNUSED(x) (void)(x)

#endif  // SRC_GUI_H_
